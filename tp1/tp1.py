import re


def flesch_reading_ease_score(n_words: int, n_sentences: int, n_syllables: int):
    """
    Computes Flesch's reading ease score as defined in https://en.wikipedia.org/wiki/Flesch–Kincaid_readability_tests
    :param n_words: int: Number of words in passage
    :param n_sentences: int: Number of sentences in passage
    :param n_syllables: int: Number of syllables in a passage

    """
    return 206.835 - 1.015 * (n_words / n_sentences) - 84.6 * (n_syllables / n_words)


def hyphenate(words: list):
    """
    Returns a list with each word being broken down with hyphenation
    
    :param words: list: Iterable of words

    """
    import pyphen
    hyphenator = pyphen.Pyphen(lang='en_US')
    return list(map(lambda w: hyphenator.inserted(w), words))


def count_syllables(hyphenated_words: list):
    """
    Counts the amount of syllables in a list of words that are already hyphenated
    :param hyphenated_words: list: List of hyphenated words

    """
    from functools import reduce
    return reduce(lambda count, hw: count + len(hw.split('-')), hyphenated_words, 0)
    

def split_by_sentence(line: str):
    """
    Returns a list of sentences, given a single line.
    :param line: str: 

    """
    # If one wants to simply get the lines without the final punctuation, one could use :
    # return re.split(r'\.+|\?|\!', line)[:-1] # Remove the last match from the list which is the remainder (either newline or empty string)
    # We capture the minimal character that follows any amount of spaces and that is terminated by a punctuation (punctuation count is at least once). A non greedy `(+?)` match is necessary to avoid matching the entire string
    a = re.findall(r'\s*(.+?[.?!]+)', line)
    return a

def split_words(line: str):
    """
    Returns a list of words. N.B: Since the solution provided considered punctuation signs (,;:?!) as words, we must consider them as words too...
    :param line: str: Line of text

    """
    w = re.findall(r'\'?\w+|[,;:?!]|\.+', line)
    return w


def compute_line_summary(line):
    """
    Generates a summary in a string format that gives the relevant information as asked.
    :param line: Line of text to be analyzed

    """
    words = split_words(line)
    
    hyphenated_words = hyphenate(words)
    n_syllables = count_syllables(hyphenated_words)
    sentences = split_by_sentence(line)
    score = flesch_reading_ease_score(len(words), len(sentences), n_syllables)
    # An easier way would be to print using print and specifying file=sys.stdout to avoid having to put our newlines
    # That would however prohibit writing the file at the end of the process
    summary = ''
    summary += "Syllables:\n{}\nTotal: {}\n\n".format('-'.join(hyphenated_words), n_syllables)
    summary += "Words:\n{}\nTotal: {}\n\n".format('-'.join(words), len(words))
    summary += "Sentences:\n{}\nTotal: {}\n\n".format('-'.join(sentences), len(sentences))
    summary += 'Total: {}\n'.format(score)
    return summary

def analyze_file(file_path):
    """
    Analyzes a file and write its output to a file
    :param file_path: Path to file to be analyzed

    """
    stats = []
    with open(file_path, "r") as f:
        for i, l in enumerate(f, 1):
            print('Line', i)
            print('------')
            summary = compute_line_summary(l)
            print(summary)
            stats.append(summary)
    fp = _get_output_file_path(file_path)
    write_output(stats, fp)


def _get_output_file_path(file_path):
    """
    Given an input file path, returns the path to a file in the same directory with _solution appended to the name of the file.
    :param file_path: Input file path

    """
    import os
    bn, ext = os.path.splitext(file_path)
    return os.path.join(os.path.dirname(bn), '{}_solution{}'.format(os.path.basename(bn), ext))
    # output_file_path = os.path.dirname(file_path)
    # output_file_name = "{}_solution.txt".format(os.path.basename(file_path)[:-4])
    # return os.path.join(output_file_path, output_file_name)


def write_output(stats, output_path):
    """
    Writes the output summaries to the output file, in the desired format.
    :param stats: List of summary
    :param output_path: Path to output file

    """
    with open(output_path, 'w') as f:
        for i, summary in enumerate(stats, 1):
            # Put spacing between items
            if i != 1:
                f.write('\n' * 2)
            line_indicator = 'Line {}\n'.format(i)
            line_sep = '-' * (len(line_indicator)-1) + '\n'
            f.writelines([line_indicator, line_sep])
            f.write(summary)

if __name__ == "__main__":
    import sys
    if len(sys.argv) < 2:
        sys.stderr.write('No input file specified\n')
        exit(1)
    analyze_file(sys.argv[1])
