# Authors
Emilio Rivera, 1689355

# Notes
- This project is based on Python 3.
- Usage of `argparse` was overlooked because it is overkill
- The module will can be used with multiple file extensions. Output will be in the same folder as the input file.
