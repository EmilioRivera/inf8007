import numpy as np

from matrix_utils import (build_distance_matrix, compute_cosine_distance,
                          normalize_matrix_per_row)


class Recommender(object):
    def __init__(self, matrix, precompute_matrix=False):
        self.m = normalize_matrix_per_row(matrix)
        self.cache = None
        if precompute_matrix:
            self.cache = build_distance_matrix(self.m)
    def closest(self, index, n=1):
        assert n >= 1
        distances = None
        if self.cache:
            distances = self.cache[i, :]
        else:
            distances = compute_cosine_distance(self.m, index)
        return np.argsort(distances)[:n], 1. - distances[np.argsort(distances)[:n]]
