import argparse
import os
from collections import ChainMap

from flask import Flask, request, send_from_directory, json
from flask.json import JSONEncoder

from td2 import build_recommendation_engine, CourseStore

parser = argparse.ArgumentParser()
parser.add_argument('-p', '--port', type=int)
defaults = {'port': 8000, 'name': 'Recommender', 'recommendation_count': 5}

ARGS = parser.parse_args()
cli_args = {key: value for key, value in vars(ARGS).items() if value}
config = ChainMap(cli_args, os.environ, defaults)
app = Flask(config['name'], static_folder='public')

def build_server_requirements(load_from_cache=True):
    """
    Instatiate required classes for the server.

    Builds the recommendation engine and course store information needed for the server.
    """
    import pickle
    if load_from_cache and os.path.exists('cache'):
        with open('cache', 'rb') as f:
            print('Loaded recommendation engine from cache')
            r, cs = pickle.load(f)
            return r, cs
    r, cs = build_recommendation_engine(use_stemming=True, subspace_dimension=50, title_modifier=0., preload=True)
    try:
        with open('cache', 'wb') as of:
            print('Saving recommendation engine to cache')
            pickle.dump((r, cs), file=of)
    except Exception as e:
        print('Failed to write cache....', e)
    finally:
        return r, cs
    

class ApiCourseResponse:
    def __init__(self, id, title, description, preprocessed, stemmed, tokenised):
        self.id, self.title, self.description, self.preprocessed, self.stemmed, self.tokenised = id, title, description, preprocessed, stemmed, tokenised

class ApiSimilarityResponse:
    def __init__(self, id, title, description, similarity):
        self.id, self.title, self.description, self.similarity = id, title, description, similarity

class MyJSONEncoder(JSONEncoder):
    """Custom JSON encoder in order to serialize our own classes."""
    def default(self, obj):
        if isinstance(obj, ApiCourseResponse):
            return {
                'id': obj.id, 
                'title': obj.title,
                'description': obj.description,
                'preprocessed': obj.preprocessed,
                'stemmed': obj.stemmed,
                'tokenised': obj.tokenised,
            }
        if isinstance(obj, ApiSimilarityResponse):
            return {
                'id': obj.id, 
                'title': obj.title,
                'description': obj.description,
                'similarity': obj.similarity,
            }
        return super(MyJSONEncoder, self).default(obj)


@app.route('/api/recommend/<course>')
def recommend_api(course):
    """ API for getting recommendations given a course name. Number of results can be parameterized by a
    query parameter, `n`.
    """
    number_of_recommendations = int(request.args.get('n', config['recommendation_count']))

    print('Asking for {} recommendations for class {}'.format(number_of_recommendations, course))
    global recommender_engine, course_store
    ci = course_store.index_for_course(course)
    closest_i, closest_v = recommender_engine.closest(ci, number_of_recommendations)
    closests = list(map(lambda i: course_store.store[i], closest_i))
    return json.jsonify([ApiSimilarityResponse(id=x[0], title=x[-3], description=x[-2], similarity=v) for x, v in zip(closests, closest_v)])

def find_course(course_name):
    """Get class information from the global store given a course name"""
    global course_store
    c = course_store.information(course_name)
    # TODO: Correctly seperate the information
    return ApiCourseResponse(c[0], c[-3], c[-2], c[-1], c[5], c[3])

@app.route('/api/transformed/<course_input>')
def transformed_api(course_input):
    """API for getting the information of a course; notably, the text used for creating the recommender system.
    
    Will return 404 for a course that is not within the CourseStore.
    """
    try:
        c = find_course(course_input)
        return json.jsonify(c)
    except IndexError as e:
        return "Le cours {} n'existe pas!".format(course_input), 404

@app.route('/')
def index():
    """Default web page to be sent on `/` is tp3.html"""
    return app.send_static_file('tp3.html')

@app.route('/<path>')
def default(path):
    """Any other request sent is resolved to a local file within the public directory."""
    print('path is', path)
    return app.send_static_file(path)

if __name__ == '__main__':
    recommender_engine, course_store  = build_server_requirements()
    app.json_encoder = MyJSONEncoder
    app.run(port=config['port'])
