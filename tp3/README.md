# Team
1720563, Louis Clouâtre-Latraverse
1689355, Emilio Rivera

# Installation and usage
Note we use python 3.4+.
Steps:
1. Make sure that a directory named `PolyHEC` is contained within the root of the project. If not, unzip in order to have `PolyHEC` folder at the same level as `td3.py`.
2. Run `pip install -r requirements.txt` to install dependencies
3. Run with your python executable the `td3.py`, ex: `python td3.py`, with the current working directory being on the same level as `td3.py`.
4. Open your browser to http://localhost:8000
5. ????
6. Profit

## Note:
On the first run, we compute all the SVD decomposition and cache it within the directory, so it might take some time to run at first.