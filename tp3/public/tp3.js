"use strict";

/*********/
/* model */
/*********/
const model = {
  currentClass: {
    id: null,
    title: null,
    description: null,
    preprocessed: null,
    stemmed: null,
    tokenised: null
  },
  recommendations: [],
  currentTab: 0
};

/**********/
/* update */
/**********/

function toggleTabs() {
  model.currentTab = +!model.currentTab;
  tabView();
  contentView();
}

/**
 * Le api doit retourner un object json sous le format:
 * {
 *    id: str
 *    title: str
 *    description: str
 *    preprocessed: List[int]
 *    stemmed: List[str]
 *    tokenised: List[str]
 * }
 **/
async function updateClass() {
  const id = document.querySelector("#user-input").value;
  if (id && !id.match(/^\s+$/)) {
    const resp = await fetch("/api/transformed/" + id);
    if (resp.ok) {
      const json = await resp.json();
      model.currentClass = {
        id: json.id,
        title: json.title,
        description: json.description,
        preprocessed: json.preprocessed,
        stemmed: json.stemmed,
        tokenised: json.tokenised
      };
      model.recommendations = [];
      classView();
      contentView();
      getRecommendations();
    } else {
      const err = await resp.text();
      showError(err);
    }
  }
}

/**
 * Le api doit retourner un object json sous le format:
 * [{
 *    id: str
 *    title: str
 *    description: str
 *    similarity: float
 * }]
 **/
async function getRecommendations() {
  const n = model.recommendations.length ? model.recommendations.length + 1 : 5;
  const resp = await fetch(
    "/api/recommend/" + model.currentClass.id + "?n=" + n
  );
  if (resp.ok) {
    const json = await resp.json();
    model.recommendations = json;
    recommendationsView();
  } else {
    const err = await resp.text();
    showError(err);
  }
}

/********/
/* view */
/********/

function tabView() {
  const tabs = document.querySelectorAll(".tab");
  if (model.currentTab) {
    tabs[0].classList.remove("tab--active");
    tabs[1].classList.add("tab--active");
  } else {
    tabs[0].classList.add("tab--active");
    tabs[1].classList.remove("tab--active");
  }
}

function contentView() {
  const contents = document.querySelectorAll(".content");
  if (model.currentTab && model.currentClass.id) {
    contents[0].classList.add("content--hidden");
    contents[1].classList.remove("content--hidden");
  } else if (model.currentClass.id) {
    contents[0].classList.remove("content--hidden");
    contents[1].classList.add("content--hidden");
  }
}

function classView() {
  document.querySelector("#user-input").removeAttribute("style");
  const class_ = document.querySelector("#searched-class");
  class_.style.visibility = "visible";

  const title = class_.querySelector(".title");
  if (!title.querySelectorAll("div").length) {
    title.append(document.createElement("div"));
    title.append(document.createElement("div"));
  }
  const titleDivs = title.querySelectorAll("div");
  titleDivs[0].textContent = model.currentClass.id.toUpperCase();
  titleDivs[1].textContent = model.currentClass.title;

  class_.querySelector(".description").textContent =
    model.currentClass.description;

  document.querySelector(
    "#tokenised"
  ).textContent = model.currentClass.tokenised.join(", ");

  document.querySelector(
    "#stemmed"
  ).textContent = model.currentClass.stemmed.join(", ");

  document.querySelector(
    "#preprocessed"
  ).textContent = model.currentClass.preprocessed.join(", ");
}

function toggleShowDescription(node) {
  const description = node.querySelector(".description");
  if (description.classList.contains("description--showing")) {
    description.classList.remove("description--showing");
  } else {
    description.classList.add("description--showing");
  }
}

function showError(err) {
  console.log(err);
  const input = document.querySelector("#user-input");
  const error = document.querySelector("#error");
  input.style.border = "1px solid #b30000";
  if (err) {
    error.textContent = err;
  }
  const removeError = event => {
    if (event.key !== "Enter") {
      input.removeAttribute("style");
      error.textContent = "";
      input.removeEventListener("keydown", removeError);
    }
  };
  input.addEventListener("keydown", removeError);
}

function recommendationsView() {
  const recommendations = document.querySelector("#recommendations");
  clear(recommendations);
  model.recommendations.map(recommendation => {
    recommendations.append(recommendationView(recommendation));
  });
}

function recommendationView(recommendation) {
  const div = document.createElement("div");
  div.classList.add("foldable");
  div.addEventListener("click", () => toggleShowDescription(div));

  const title = document.createElement("div");
  title.classList.add("title");
  const id = document.createElement("id");
  id.textContent = recommendation.id.toUpperCase();
  title.append(id);
  const title_ = document.createElement("div");
  title_.textContent = recommendation.title;
  title.append(title_);
  const similarity = document.createElement("div");
  similarity.textContent = "(" + recommendation.similarity.toFixed(3) + ")";
  title.append(similarity);
  div.append(title);

  const description = document.createElement("div");
  description.classList.add("description");
  description.textContent = recommendation.description;
  div.append(description);

  return div;
}

/*************/
/* listeners */
/*************/

function initListeners() {
  initTabListeners();
  initClassIdInputListener();
  initFoldingListeners();
  initMoreRecommendationsListener();
}

function initTabListeners() {
  document.querySelectorAll(".tab").forEach(tab => {
    tab.addEventListener("click", toggleTabs);
  });
}

function initClassIdInputListener() {
  document.querySelector("#user-input").addEventListener("keypress", event => {
    if (event.key == "Enter") {
      updateClass();
    }
  });
}

function initFoldingListeners() {
  document.querySelectorAll(".foldable").forEach(fold => {
    fold.addEventListener("click", () => toggleShowDescription(fold));
  });
}

function initMoreRecommendationsListener() {
  document
    .querySelector("#add-recommendation")
    .addEventListener("click", getRecommendations);
}

/********/
/* init */
/********/

function init() {
  initListeners();
}

/*********/
/* utils */
/*********/

function clear(node) {
  while (node.hasChildNodes()) {
    node.removeChild(node.lastChild);
  }
}

window.onload = init;
