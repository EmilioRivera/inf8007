from collections import Counter

import numpy as np


class Vectorizer(object):
    def __init__(self):
        self.word_to_ix = {}
        self.ix_to_word = {}
        self.word_counter = Counter()

    def update(self, words):
        for w in words:
            if w not in self.word_to_ix:
                new_index = len(self.word_to_ix)
                self.word_to_ix[w] = new_index
                self.ix_to_word[new_index] = w
                # self.word_counter[w] = 1
            # else:
                # self.word_counter[w] += 1
            self.word_counter.update([w])

    def occurences_index(self, words):
        occ_dict = {}
        cw = Counter(words)
        for w, count in cw.items():
            if w not in self.word_to_ix:
                raise IndexError('Word {} was not treated yet and does not figure in current vocabulary'.format(w))
            occ_dict[self.word_to_ix[w]] = count
        return occ_dict

    def occurences_index_np(self, words):
        occurences = self.occurences_index(words)
        occ = np.zeros(self.count())
        for i, c in occurences.items():
            occ[i] = c
        return occ

    def __str__(self):
        s = 'Word Counter: {}\n'.format(str(self.word_counter))
        s += 'Index to word mapper {}\n'.format(str(self.ix_to_word))
        return s

    def count(self):
        return len(self.word_to_ix)
