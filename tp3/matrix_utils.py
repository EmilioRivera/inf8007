import numpy as np


def build_distance_matrix(normalized_matrix):
    return 1. - np.dot(normalized_matrix, normalized_matrix.T)

def compute_cosine_distance(normalized_matrix, index):
    return 1. - np.dot(normalized_matrix[index, :], normalized_matrix.T)

def construct_tf_idf(matrix):
    idf = np.log(float(matrix.shape[0])) / (matrix != 0).sum(axis=0)
    return matrix.multiply(idf)


def normalize_matrix_per_row(matrix):
    import numpy as np
    # We divided by the row norm. Reshape is required to avoid numpy complaining about missing second dimension
    return matrix / np.sqrt(np.sum(matrix**2, axis=1)).reshape(-1, 1)


def decompose_matrix(matrix, k):
    from scipy.sparse.linalg import svds
    u, s, vt = svds(matrix, k=k)
    return u
