import argparse
import os
import re
from collections import Counter

import numpy as np
from nltk.stem.snowball import FrenchStemmer
from scipy import sparse

from recommender import Recommender
from vectorizer import Vectorizer
from matrix_utils import compute_cosine_distance, build_distance_matrix, construct_tf_idf, decompose_matrix

parser = argparse.ArgumentParser()

parser.add_argument('-s', '--size', type=int, help="Dimension of the subspace to be created via singular value decomposition (SVD)", default=10)
parser.add_argument('-k', '--number-of-matches', type=int, help="Number of classes to return from the recommendation system")
parser.add_argument('-tm', '--title-multiplier', type=float, help="Multiplication factor to words in the title. If not provided, words in title are ignored in computation. Value of 1.0 will make it so as words in title have the same weight as words in description.", default=0.0)
parser.add_argument('--stem', action='store_true', help="Use word stemming for analysis", default=False)
parser.add_argument('classname')


title_finder_re = re.compile(r'^\s*TitreCours: (.*)\s*Description', re.MULTILINE)
description_finder_re = re.compile(r'.*DescriptionCours: (.*)$', re.MULTILINE)


def _extract_name_and_description(file_content):
    title_matches = re.findall(title_finder_re, file_content)
    description_matches = re.findall(description_finder_re, file_content)
    title = "" if len(title_matches) == 0 else title_matches[0]
    description = "" if len(description_matches) == 0 else description_matches[0]
    return (title, description)


def clean_string_for_processing(line):
    lower_line = line.lower()
    lower_line = re.sub(r'&oelig;', 'oe', lower_line)
    lower_line = re.sub(r'&nbsp;|\s+', ' ', lower_line)
    lower_line = re.sub(r'&rsquo;', '\'', lower_line)
    return lower_line.strip()   


def split_words(line: str):
    """
    Returns a list of words. N.B: Since the solution provided considered punctuation signs (,;:?!) as words, we must consider them as words too...
    :param line: str: Line of text

    """
#     w = re.findall(r'(?:\'|&\w{3,5}?;)?\w+', line)
    w = re.split(r' +|[ :;,.\'()\\]', line)
    
    return [a for a in w if len(a) != 0 ]


def _extract_class_name(file_path):
    path, ext = os.path.splitext(file_path)
    return os.path.basename(path)


def get_files(dir='PolyHEC'):
    for f in os.listdir(dir):
        target = os.path.join(dir, f)
        if os.path.isdir(target):
            continue
        yield target


def construct_matrix(descriptions, vectorizer, title_modifier=0.0):
    if title_modifier == 0.0:
        print('Not considering title in matrix')
    else:
        print('Considering words in title with weight {}'.format(title_modifier))
    mx = sparse.dok_matrix((len(descriptions), vectorizer.count()))
    for i, (_, _, _, _, dts, des, _, _, _) in enumerate(descriptions):
        # Only build a vector for words in the description
        oc_ld = vectorizer.occurences_index(des)
        for wi, wc in oc_ld.items():
            mx[i, wi] = wc
        if title_modifier != 0.0:
            oc_title_dict = vectorizer.occurences_index(dts)
            for wi, wc in oc_title_dict.items():
                mx[i, wi] = wc * title_modifier
    # assert sum([len(d[3]) for d in descriptions]) == sum(vectorizer.word_counter.values()) == mx.sum()
    return mx


class CourseStore:
    def __init__(self, use_stemming, accept_preload=False):
        import pickle
        hyper_name = "stem_{}".format(use_stemming)
        cache_exists = os.path.exists(hyper_name)
        if accept_preload and cache_exists:
            with open(hyper_name, 'rb') as f:
                self.store = pickle.load(f)
                print("Loaded from cache")
        else:
            self.store = CourseStore.aggregate_classes_info(use_stemming)
        if not cache_exists:
            try:
                with open(hyper_name, 'wb') as f:
                    pickle.dump(self.store, f)
            except Exception as e:
                print("error while trying to save cache", e)
    
    @staticmethod
    def aggregate_classes_info(use_stemming):
        classes_info = []
        stemmer = FrenchStemmer() if use_stemming else None    
        for file_path in get_files():
            with open(file_path, 'r') as file:
                # print('Parsing file {}'.format(file_path))
                cn = _extract_class_name(file_path)
                title, desc = _extract_name_and_description(file.read())

                title_c, desc_c = clean_string_for_processing(title), clean_string_for_processing(desc)
                words_in_title, words_in_doc = split_words(title_c), split_words(desc_c)
                if use_stemming:
                    words_in_title_stem = list(map(lambda d: stemmer.stem(d) if use_stemming else d, words_in_title))
                    words_in_doc_stem = list(map(lambda d: stemmer.stem(d) if use_stemming else d, words_in_doc))

                classes_info.append([cn, title_c, words_in_title, words_in_doc, words_in_title_stem, words_in_doc_stem, title, desc, None])
        return classes_info

    def information(self, course_name):
        return [co for co in self.store if co[0].lower() == course_name.lower()][0]

    def index_for_course(self, course_name):
        return next((i for i, c in enumerate(self.store) if (c[0]).lower() == course_name.lower()), None)

def recommend(recommender, course_store, course_name, number_of_recommendations):
    index = next((i for i, c in enumerate(course_store.store) if (c[0]).lower() == course_name.lower()), None)
    closest_index = recommender.closest(index, n=number_of_recommendations)
    closest_courses_name = [course_store.store[int(i)][0] for i in closest_index]
    print("Closest courses to {} are {}".format(course_name, closest_courses_name))


def build_recommendation_engine(use_stemming=False, subspace_dimension=10, title_modifier=None, preload=False):
    cs = CourseStore(use_stemming, accept_preload=preload)
    classes_info = cs.store

    vectorizer = Vectorizer()
    for (cn, t, wt, wd, wts, wds, _, _, _) in classes_info:
        vectorizer.update(wds)
        if title_modifier and title_modifier != 0.0:
            vectorizer.update(wts)

    document_x_words = construct_matrix(classes_info, vectorizer, title_modifier=title_modifier).tocsr()
    for i in range(document_x_words.shape[0]):
        cs.store[i][-1] = document_x_words[i].nonzero()[1].tolist()

    tf_idf = construct_tf_idf(document_x_words)

    decomposed_tf_idf = decompose_matrix(tf_idf, k=subspace_dimension)
    r = Recommender(decomposed_tf_idf, precompute_matrix=False)
    return r, cs


if __name__ == "__main__":
    ARGS = parser.parse_args()
    r, cs = build_recommendation_engine(use_stemming=ARGS.stem, subspace_dimension=ARGS.size, preload=True, title_modifier=ARGS.title_multiplier)
    recommend(r, cs, ARGS.classname, number_of_recommendations=ARGS.number_of_matches)
