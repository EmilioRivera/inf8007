import os
import re
import argparse
import numpy as np
from scipy import sparse
from collections import Counter
from nltk.stem.snowball import FrenchStemmer

parser = argparse.ArgumentParser()

parser.add_argument('-s', '--size', type=int, help="Dimension of the subspace to be created via singular value decomposition (SVD)", default=10)
parser.add_argument('-k', '--number-of-matches', type=int, help="Number of classes to return from the recommendation system")
parser.add_argument('-tm', '--title-multiplier', type=float, help="Multiplication factor to words in the title. If not provided, words in title are ignored in computation. Value of 1.0 will make it so as words in title have the same weight as words in description.", default=0.0)
parser.add_argument('--stem', action='store_true', help="Use word stemming for analysis", default=False)
parser.add_argument('classname')


title_finder_re = re.compile(r'^\s*TitreCours: (.*)\s*Description', re.MULTILINE)
description_finder_re = re.compile(r'.*DescriptionCours: (.*)$', re.MULTILINE)


def _extract_name_and_description(file_content):
    title_matches = re.findall(title_finder_re, file_content)
    description_matches = re.findall(description_finder_re, file_content)
    title = "" if len(title_matches) == 0 else title_matches[0]
    description = "" if len(description_matches) == 0 else description_matches[0]
    return (title, description)


def clean_string_for_processing(line):
    lower_line = line.lower()
    lower_line = re.sub(r'&oelig;', 'oe', lower_line)
    lower_line = re.sub(r'&nbsp;|\s+', ' ', lower_line)
    lower_line = re.sub(r'&rsquo;', '\'', lower_line)
    return lower_line.strip()   


def split_words(line: str):
    """
    Returns a list of words. N.B: Since the solution provided considered punctuation signs (,;:?!) as words, we must consider them as words too...
    :param line: str: Line of text

    """
#     w = re.findall(r'(?:\'|&\w{3,5}?;)?\w+', line)
    w = re.split(r' +|[ :;,.\'()\\]', line)
    
    return [a for a in w if len(a) != 0 ]


def _extract_class_name(file_path):
    path, ext = os.path.splitext(file_path)
    return os.path.basename(path)


def get_files(dir='PolyHEC'):
    for f in os.listdir(dir):
        target = os.path.join(dir, f)
        if os.path.isdir(target):
            continue
        yield target
        

class Vectorizer(object):
    def __init__(self):
        self.word_to_ix = {}
        self.ix_to_word = {}
        self.word_counter = Counter()

    def update(self, words):
        for w in words:
            if w not in self.word_to_ix:
                new_index = len(self.word_to_ix)
                self.word_to_ix[w] = new_index
                self.ix_to_word[new_index] = w
                # self.word_counter[w] = 1
            # else:
                # self.word_counter[w] += 1
            self.word_counter.update([w])

    def occurences_index(self, words):
        occ_dict = {}
        cw = Counter(words)
        for w, count in cw.items():
            if w not in self.word_to_ix:
                raise IndexError('Word {} was not treated yet and does not figure in current vocabulary'.format(w))
            occ_dict[self.word_to_ix[w]] = count
        return occ_dict

    def occurences_index_np(self, words):
        occurences = self.occurences_index(words)
        occ = np.zeros(self.count())
        for i, c in occurences.items():
            occ[i] = c
        return occ

    def __str__(self):
        s = 'Word Counter: {}\n'.format(str(self.word_counter))
        s += 'Index to word mapper {}\n'.format(str(self.ix_to_word))
        return s

    def count(self):
        return len(self.word_to_ix)


def construct_matrix(descriptions, vectorizer, title_modifier=0.0):
    if title_modifier == 0.0:
        print('Not considering title in matrix')
    else:
        print('Considering words in title with weight {}'.format(title_modifier))
    mx = sparse.dok_matrix((len(descriptions), vectorizer.count()))
    for i, (_, _, dt, de) in enumerate(descriptions):
        # Only build a vector for words in the description
        oc_ld = vectorizer.occurences_index(de)
        for wi, wc in oc_ld.items():
            mx[i, wi] = wc
        if title_modifier != 0.0:
            oc_title_dict = vectorizer.occurences_index(dt)
            for wi, wc in oc_title_dict.items():
                mx[i, wi] = wc * title_modifier
    # assert sum([len(d[3]) for d in descriptions]) == sum(vectorizer.word_counter.values()) == mx.sum()
    return mx


def aggregate_classes_info(use_stemming):
    classes_info = []
    stemmer = FrenchStemmer() if use_stemming else None    
    for file_path in get_files():
        with open(file_path, 'r') as file:
            # print('Parsing file {}'.format(file_path))
            cn = _extract_class_name(file_path)
            title, desc = _extract_name_and_description(file.read())
            title, desc = clean_string_for_processing(title), clean_string_for_processing(desc)
            words_in_title, words_in_doc = split_words(title), split_words(desc)
            if use_stemming:
                words_in_title = list(map(lambda d: stemmer.stem(d) if use_stemming else d, words_in_title))
                words_in_doc = list(map(lambda d: stemmer.stem(d) if use_stemming else d, words_in_doc))
            classes_info.append((cn, title, words_in_title, words_in_doc))
    return classes_info

def main(course_name, use_stemming=False, subspace_dimension=10, number_of_recommendations=1, title_modifier=None, preload=False):
    hyper_name = "stem_{}".format(use_stemming)
    cache_exists = os.path.exists(hyper_name)
    import pickle
    if preload and cache_exists:
        with open(hyper_name, 'rb') as f:
            classes_info = pickle.load(f)
            print("Loaded from cache")
    else:
        classes_info = aggregate_classes_info(use_stemming)
    if not cache_exists:
        try:
            with open(hyper_name, 'wb') as f:
                pickle.dump(classes_info, f)
        except Exception as e:
            print("error while trying to save cache", e)
    # Find the index for our course name
    index = next((i for i, c in enumerate(classes_info) if (c[0]).lower() == course_name.lower()), None)
    if index is None:
        print("Class {} was not found. exiting :( ".format(course_name))
        exit(1)
    # print("Index for class {} is {}".format(course_name, index))
    vectorizer = Vectorizer()
    for (cn, t, wt, wd) in classes_info:
        vectorizer.update(wd)
        if title_modifier and title_modifier != 0.0:
            vectorizer.update(wt)

    document_x_words = construct_matrix(classes_info, vectorizer, title_modifier=title_modifier).tocsr()
    tf_idf = construct_tf_idf(document_x_words)

    decomposed_tf_idf = decompose_matrix(tf_idf, k=subspace_dimension)
    r = Recommender(decomposed_tf_idf, precompute_matrix=False)
    closest_index = r.closest(index, n=number_of_recommendations)
    closest_courses_name = [classes_info[int(i)][0] for i in closest_index]
    print("Closest courses to {} are {}".format(course_name, closest_courses_name))
    return r

class Recommender(object):
    def __init__(self, matrix, precompute_matrix=False):
        self.m = normalize_matrix_per_row(matrix)
        self.cache = None
        if precompute_matrix:
            self.cache = build_distance_matrix(self.m)
    def closest(self, index, n=1):
        assert n >= 1
        distances = None
        if self.cache:
            distances = self.cache[i, :]
        else:
            distances = compute_cosine_distance(self.m, index)
        return np.argsort(distances)[:n]


def construct_tf_idf(matrix):
    idf = np.log(float(matrix.shape[0])) / (matrix != 0).sum(axis=0)
    return matrix.multiply(idf)


def normalize_matrix_per_row(matrix):
    import numpy as np
    # We divided by the row norm. Reshape is required to avoid numpy complaining about missing second dimension
    return matrix / np.sqrt(np.sum(matrix**2, axis=1)).reshape(-1, 1)


def decompose_matrix(matrix, k):
    from scipy.sparse.linalg import svds
    u, s, vt = svds(matrix, k=k)
    return u


def build_distance_matrix(normalized_matrix):
    return 1. - np.dot(normalized_matrix, normalized_matrix.T)

def compute_cosine_distance(normalized_matrix, index):
    return 1. - np.dot(normalized_matrix[index, :], normalized_matrix.T)

if __name__ == "__main__":
    ARGS = parser.parse_args()
    main(ARGS.classname, use_stemming=ARGS.stem, subspace_dimension=ARGS.size, number_of_recommendations=ARGS.number_of_matches, preload=True, title_modifier=ARGS.title_multiplier)
